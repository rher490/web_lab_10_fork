-- Answers to Exercise 8 here
SELECT * FROM lab10exercise02;
SELECT * FROM lab10exercise03Members;
SELECT * FROM lab10exercise04;
SELECT * FROM  lab10exercise05;
SELECT * FROM  lab10exercise06MoviesRented;
SELECT * FROM  lab10exercise07ArticleComments;

DELETE FROM lab10exercise05 WHERE first_name = 'Bill';
SELECT * FROM  lab10exercise05;

ALTER TABLE lab10exercise05 DROP COLUMN last_name;
SELECT * FROM  lab10exercise05;

DROP TABLE IF EXISTS lab10exercise05;

UPDATE lab10exercise05 SET first_name='Bill2' WHERE username='programmer2';
UPDATE lab10exercise07ArticleComments SET id_code=5 WHERE comments='Potato potato';
SELECT * FROM  lab10exercise07ArticleComments;

SELECT * FROM  lab10exercise05;
