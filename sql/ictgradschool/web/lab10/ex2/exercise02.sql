-- Answers to Exercise 2 here
drop table IF EXISTS lab10exercise02;

CREATE TABLE IF NOT EXISTS lab10exercise02 (
  username   VARCHAR(20),
  first_name VARCHAR(20),
  last_name  VARCHAR(100),
  email      TEXT,
  PRIMARY KEY (username)
);

INSERT INTO lab10exercise02 VALUES
  ('programmer1','Bill', 'Gates','bill@microsoft.com'),
  ('spiderman','Peter', 'Parker','pete89@live.com'),
  ('progeny','Pete', 'Tier','petey23@gmail.com'),
  ('spooks','Katie', 'Peterson','kappa@gmail.com');

SELECT * FROM lab10exercise02;