-- Answers to Exercise 3 here

DROP TABLE IF EXISTS lab10exercise03Members;

CREATE TABLE IF NOT EXISTS lab10exercise03Members (
  name VARCHAR(100) NOT NULL,
  gender VARCHAR(6) NOT NULL,
  year_born INT,
  joined CHAR(4),
  num_hires INT,
  PRIMARY KEY (name)
);

INSERT INTO lab10exercise03Members Values ( 'Peter Jackson', 'male', 1961, '1997', 17000);
INSERT INTO lab10exercise03Members Values ( 'Jane Campion', 'female', 1954, '1980', 30000);
INSERT INTO lab10exercise03Members Values ( 'Roger Donaldson', 'male', 1945, '1980', 12000);
INSERT INTO lab10exercise03Members Values ( 'Temuera Morrison', 'male', 1960, '1995', 15500);
INSERT INTO lab10exercise03Members Values ( 'Russell Crowe', 'male', 1964, '1990', 10000);
INSERT INTO lab10exercise03Members Values ( 'Lucy Lawless', 'female', 1968, '1995', 5000);
INSERT INTO lab10exercise03Members Values ( 'Michael Hurst', 'male', 1957, '2000', 15000);
INSERT INTO lab10exercise03Members Values ( 'Andrew Niccol', 'male', 1964, '1997', 3500);
INSERT INTO lab10exercise03Members Values ( 'Kiri Te Kanawa', 'female', 1944, '1997', 500);
INSERT INTO lab10exercise03Members Values ( 'Lorde' , 'female', 1996, '2010', 1000);
INSERT INTO lab10exercise03Members Values ( 'Scribe' , 'male', 1979, '2000', 5000);
INSERT INTO lab10exercise03Members Values ( 'Kimbra' , 'female', 1990, '2005', 7000);
INSERT INTO lab10exercise03Members Values ( 'Neil Finn', 'male', 1958, '1985', 6000);
INSERT INTO lab10exercise03Members Values ( 'Anika Moa', 'female', 1980, '2000', 700);
INSERT INTO lab10exercise03Members Values ( 'Bic Runga', 'female', 1976, '1995', 5000);
INSERT INTO lab10exercise03Members Values ( 'Ernest Rutherford', 'male', 1871, '1930', 4200);
INSERT INTO lab10exercise03Members Values ( 'Kate Sheppard', 'female', 1847, '1930', 1000);
INSERT INTO lab10exercise03Members Values ( 'Apirana Turupa Ngata', 'male', 1874, '1920', 3500);
INSERT INTO lab10exercise03Members Values ( 'Edmund Hillary', 'male', 1919, '1955', 10000);
INSERT INTO lab10exercise03Members Values ( 'Katherine Mansfield', 'female', 1888, '1920', 2000);
INSERT INTO lab10exercise03Members Values ( 'Margaret Mahy', 'female', 1936, '1985', 5000);
INSERT INTO lab10exercise03Members Values ( 'John Key', 'male', 1961, '1990', 20000);
INSERT INTO lab10exercise03Members Values ( 'Sonny Bill Williams', 'male', 1985, '1995', 15000);
INSERT INTO lab10exercise03Members Values ( 'Dan Carter', 'male', 1982, '1990', 20000);
INSERT INTO lab10exercise03Members Values ( 'Bernice Mene', 'female', 1975, '1990', 30000);
