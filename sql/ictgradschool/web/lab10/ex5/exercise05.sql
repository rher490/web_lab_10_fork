-- Answers to Exercise 5 here
drop table IF EXISTS lab10exercise05;

CREATE TABLE IF NOT EXISTS lab10exercise05 (
username   VARCHAR(20),
first_name VARCHAR(20),
last_name  VARCHAR(100),
email      TEXT,
PRIMARY KEY (username)
);

INSERT INTO lab10exercise05 VALUES
('programmer1','Bill', 'Gates','bill@microsoft.com'),
('spiderman','Peter', 'Parker','pete89@live.com'),
('progeny','Pete', 'Tier','petey23@gmail.com'),
('spooks','Katie', 'Peterson','kappa@gmail.com');

SELECT * FROM lab10exercise05;

INSERT INTO lab10exercise05 VALUE ('programmer2','Bill', 'Gates','bill@microsoft.com');