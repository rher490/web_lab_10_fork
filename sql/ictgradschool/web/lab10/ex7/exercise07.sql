-- Answers to Exercise 7 here
DROP TABLE IF EXISTS lab10exercise07ArticleComments;

CREATE TABLE IF NOT EXISTS lab10exercise07ArticleComments (
  articleId INT NOT NULL,
  comments  TEXT,
  id_code   INT AUTO_INCREMENT,
  PRIMARY KEY (id_code),
  FOREIGN KEY (articleId) REFERENCES lab10exercise04 (id)
);

INSERT INTO lab10exercise07ArticleComments(articleId, comments)  VALUES (1, 'Is this lorem upsum?'),
  (1, 'I think you mean Lorem Ipsum'),
  (1, 'Potato potato'),
  (1, 'You''re saying it wrong. It''s Levi-OOO-sa, not Levio-SA');

SELECT * FROM lab10exercise07ArticleComments;